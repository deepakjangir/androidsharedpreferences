var exec = require('cordova/exec');

exports.setSharedPreferences = function (arg0, success, error) {
    exec(success, error, 'AndroidSharedPreferences', 'setSharedPreferences', [arg0]);
};
exports.getSharedPreferences = function (arg0, success, error) {
    exec(success, error, 'AndroidSharedPreferences', 'getSharedPreferences', [arg0]);
};

exports.clearValue = function (arg0, success, error) {
    exec(success, error, 'AndroidSharedPreferences', 'clearValue', [arg0]);
};
